@extends('layout.utama')
@section('Judul')
    Halaman Form Sign Up
@endsection    
@section('Isi')
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign UP</a></li>
        <li>Selesai!</li>
    </ol>
@endsection