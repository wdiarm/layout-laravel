@extends('layout.utama')
@section('Judul')
    List Data Para Pemain Film
@endsection    
@section('Isi')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Nama Pemain</label>
        <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur Pemain</label>
        <input type="text" class="form-control" name="umur" id="body" placeholder="Masukkan Body">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio Pemain</label>
        <input type="text" class="form-control" name="bio" id="body" placeholder="Masukkan Body">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection