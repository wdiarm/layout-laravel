@extends('layout.utama')
@section('Judul')
    Pembaruan Data Pemain {{$cast->id}}
@endsection    
@section('Isi')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama Pemain</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur Pemain</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->Umur}}" id="body" placeholder="Masukkan Body">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio Pemain</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="body" placeholder="Masukkan Body">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit Data</button>
</form>

@endsection