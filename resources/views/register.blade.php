@extends('layout.utama')
@section('Judul')
    Halaman Form Data Diri
@endsection    
@section('Isi')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="namafir"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text" name="namalast"> <br> <br>

        <label>Gender</label> <br> <br>
        <input type="radio" name="e">Male <br> 
        <input type="radio" name="e">Female <br>
        <input type="radio" name="e">Other <br> <br>

        <label>Nationality:</label> <br> <br>
        <select name="Nasional">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapure">Singapure</option>
            <option value="Thailand">Thailand</option>
        </select> <br> <br>

        <Label>Language Spoken:</Label> <br> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br> <br>

        <label>Bio:</label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">

    </form>
@endsection